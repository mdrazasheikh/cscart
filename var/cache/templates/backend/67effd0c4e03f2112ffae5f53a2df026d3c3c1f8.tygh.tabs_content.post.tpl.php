<?php /* Smarty version Smarty-3.1.21, created on 2017-07-07 19:32:01
         compiled from "C:\xampp\htdocs\cscart\design\backend\templates\addons\new_arrival\hooks\products\tabs_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:81722023595f9c32ecf612-16080946%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '67effd0c4e03f2112ffae5f53a2df026d3c3c1f8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cscart\\design\\backend\\templates\\addons\\new_arrival\\hooks\\products\\tabs_content.post.tpl',
      1 => 1499445119,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '81722023595f9c32ecf612-16080946',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_595f9c32f1ded7_96951033',
  'variables' => 
  array (
    'product_data' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_595f9c32f1ded7_96951033')) {function content_595f9c32f1ded7_96951033($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('new_arrival','is_new'));
?>
<?php $_smarty_tpl->tpl_vars["data"] = new Smarty_variable($_smarty_tpl->tpl_vars['product_data']->value, null, 0);?>

<div id="content_new_arrival" class="hidden">
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("new_arrival"),'target'=>"#new_arrival_products_hook"), 0);?>

    <div id="new_arrival_products_hook">
        <fieldset>
        <div class="control-group">
                <label class="control-label" for="is_new"><?php echo $_smarty_tpl->__("is_new");?>
</label>
                <div class="controls">
                    <input type="hidden" name="product_data[is_new]" value="N" />
                    <input type="checkbox" name="product_data[is_new]" id="is_new" value="Y" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_new']=="Y") {?>checked="checked"<?php }?>>
                </div>
            </div>
        </fieldset>
    </div>
</div><?php }} ?>

<?php
if (! defined ( 'AREA' )) {
	die ( 'no access' );
}
function fn_advanced_addon_get_category_data_pre($category_id, $fields_list, $get_main_pair, $skip_company_conditions, $lang_code) {
	$auth = $_SESSION ['auth'];
	
	if (! empty ( $auth ['user_id'] ) && AREA == 'C') {
		$viewed_categories = db_get_field ( 'select category from ?:advanced_addon_data where user_id = ?i', $auth ['user_id'] );
		
		if (! empty ( $viewed_categories )) {
			$viewed_categories = unserialize ( $viewed_categories );
		}
		$viewed_categories [$category_id] = true;
		$viewed_categories = serialize ( $viewed_categories );
		
		db_query ( 'REPLACE INTO ?:advanced_addon_data VALUES (?i, ?s)', $auth ['user_id'], $viewed_categories );
	}
}
<?php
if ((! defined ( 'BOOTSTRAP' ))) {
	die ( 'Access Denied' );
}

function fn_new_arrival_get_product_data(&$product_id, &$field_list, &$join) {

	$field_list .= ', ?:new_arrival.is_new';
	$join .= " LEFT JOIN ?:new_arrival ON cscart_new_arrival.product_id = ?:products.product_id";

}

function fn_new_arrival_update_product_post(&$product_data, $product_id) {

	if (isset ( $product_data ['is_new'] )) {
		fn_save_new_arrival ( $product_id, $product_data ['is_new']);
	} else {
		fn_save_new_arrival ( $product_id, "N" );
	}

}

function fn_save_new_arrival($product_id, $is_new = "N") {

	$insert_id = db_get_field ( "SELECT id FROM ?:new_arrival WHERE product_id = ?i", $product_id );
	
	if (empty ( $insert_id )) {
		db_query ( "INSERT INTO ?:new_arrival (product_id, is_new) VALUES ({$product_id}, '{$is_new}')" );
	} else {
		db_query ( "UPDATE ?:new_arrival SET is_new = '{$is_new}' WHERE id = {$insert_id}" );
	}
	return true;

}

function fn_new_arrival_delete_product_post(&$product_id) {

	db_query ( "DELETE FROM ?:new_arrival WHERE product_id ={$product_id}" );

}

function fn_new_arrival_clone_product(&$product_id, &$pid) {

	$is_new = db_get_field ( "SELECT is_new from ?:new_arrival WHERE product_id = {$product_id}" );
	
	db_query ( "INSERT INTO ?:new_arrival (product_id, is_new) VALUES ({$pid}, '{$is_new}')" );

}

function fn_new_arrival_get_products(&$params, &$fields, &$sortings, &$condition, &$join) {
	

}
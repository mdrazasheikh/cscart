<?php
if (! defined ( 'BOOTSTRAP' )) {
	die ( 'Access denied' );
}

fn_register_hooks ( 'get_product_data', 'delete_product_post', 'clone_product', 'update_product_post', 'get_products' );
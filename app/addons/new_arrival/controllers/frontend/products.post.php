<?php
if (! defined ( 'BOOTSTRAP' )) {
	die ( 'Access denied' );
}

if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
	return;
}

$product = Tygh::$app ['view']->getTemplateVars ( 'product' );

if (! empty ( $product ['is_new'] ) && $product ['is_new'] == 'Y') {
	$product ['new_arrival'] = __("new_arrival");
// 	$product ['new_arrival'] = "New Arrival";
} else {
	$product ['new_arrival'] = '';
}

Tygh::$app ['view']->assign ( 'product', $product );
{assign var="data" value=$product_data}

<div id="content_new_arrival" class="hidden">
    {include file="common/subheader.tpl" title=__("new_arrival") target="#new_arrival_products_hook"}
    <div id="new_arrival_products_hook">
        <fieldset>
        <div class="control-group">
                <label class="control-label" for="is_new">{__("is_new")}</label>
                <div class="controls">
                    <input type="hidden" name="product_data[is_new]" value="N" />
                    <input type="checkbox" name="product_data[is_new]" id="is_new" value="Y" {if $data.is_new == "Y"}checked="checked"{/if}>
                </div>
            </div>
        </fieldset>
    </div>
</div>
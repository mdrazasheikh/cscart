<?php
if ($_SERVER ['REMOTE_ADDR'] = '127.0.0.1') {
	define('DEBUG_MODE', true);
	define ( 'DEVELOPMENT', true );
	error_reporting ( E_ALL );
	ini_set ( 'display_errors', 'on' );
	ini_set ( 'display_startup_errors', true );
	
	$config ['tweaks'] = array (
			'disable_block_cache' => true 
	);
	
	$config ['db_host'] = 'localhost';
	$config ['db_name'] = 'cscart';
	$config ['db_user'] = 'root';
	$config ['db_password'] = '';
}